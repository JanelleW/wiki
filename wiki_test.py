import pytest  # type: ignore
import wiki
import pathlib

CURRENT_DIR = pathlib.Path(__file__).parent
DEFAULT_DIR = CURRENT_DIR / "test_pages"
PAGE_DIR = CURRENT_DIR / "pages"
LOG_DIR = CURRENT_DIR / "logs"


@pytest.fixture
def client():
    wiki.app.config["TESTING"] = True

    with wiki.app.test_client() as client:
        yield client


def test_import():
    assert wiki is not None


def test_homepage(client):
    resp = client.get("/")
    print(resp.data)
    assert resp.status_code == 302
    assert b"/view/" in resp.data


def test_front_page(client):
    resp1 = client.get("/view/FrontPage", follow_redirects=True)
    resp2 = client.get("/view/", follow_redirects=True)
    assert resp1.status_code == 200
    assert resp2.status_code == 200
    assert b"FrontPage" in resp1.data
    assert b"FrontPage" in resp2.data


def test_page_name(client):
    resp = client.get("/view/test_PageName")
    assert resp.status_code == 200

    assert b"This is the header." in resp.data
    assert b"This right here is a paragraph." in resp.data
    assert b"/view/FrontPage" in resp.data


def test_page_name_not_found(client):
    resp = client.get("/view/notfile")
    assert resp.status_code == 404


def test_load_page_text_file_found():
    with open(DEFAULT_DIR / "test_PageName.txt") as f:
        f_contents = f.read()
    output = wiki.load_page_text(DEFAULT_DIR / "test_PageName.txt")
    assert f_contents == output


def test_parse_page_text_file_found():
    with open(DEFAULT_DIR / "test_PageName.txt") as f:
        f_contents = f.read()
    output = wiki.parse_page_text(DEFAULT_DIR / "test_PageName.txt")
    assert output in f_contents


def test_security_script_and_post():
    with open(DEFAULT_DIR / "test_Security.txt") as f:
        f_contents = f.read()
    output = wiki.security_check(f_contents)
    assert "<script>" not in output
    assert "</script>" not in output
    assert "<post>" not in output
    assert "</post>" not in output


def test_security_script():
    with open(DEFAULT_DIR / "test_Security_2.txt") as f:
        f_contents = f.read()
    output = wiki.security_check(f_contents)
    assert "<script>" not in output
    assert "</script>" not in output


def test_security_post():
    with open(DEFAULT_DIR / "test_Security_3.txt") as f:
        f_contents = f.read()
    output = wiki.security_check(f_contents)
    assert "<post>" not in output
    assert "</post>" not in output


def test_page_api(client):
    raw_resp = client.get("/api/v1/page/PageName/get?format=raw")
    html_resp = client.get("/api/v1/page/PageName/get?format=html")
    all_resp = client.get("/api/v1/page/PageName/get")
    invalid_resp = client.get("/api/v1/page/Invalid/get")
    unsupported_resp = client.get("/api/v1/page/PageName/get?format=pdf")

    assert raw_resp.status_code == 200
    assert html_resp.status_code == 200
    assert all_resp.status_code == 200
    assert invalid_resp.status_code == 404
    assert unsupported_resp.status_code == 400

    assert b"html" in html_resp.data
    assert b"raw" in raw_resp.data
    assert b"html" and b"raw" in all_resp.data


def test_bad_handle_request(client):
    resp = client.get("/view/thispagetotallydoesntexist")
    assert resp.status_code == 404
    assert b"404: Page does not exist!" in resp.data


def test_add_page_form(client):
    resp = client.get("/edit/")
    assert resp.status_code == 200
    assert b"Enter the name of the new page" in resp.data


def test_add_page(client):
    resp = client.post(
        "/add/", follow_redirects=True, data=dict(new_page="test_AddPage")
    )
    assert resp.status_code == 200
    assert b"test_AddPage" in resp.data


def test_get_edit_form(client):
    resp = client.get(
        "/edit/test_PageName",
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert b"test_PageName" in resp.data
    assert b"Page Edit" in resp.data


def test_make_edits_valid(client):
    # To change page back to original state
    with open(PAGE_DIR / "test_PageName.txt", "r") as f:
        revert = f.read()

    # All fields submitted
    good_resp = client.post(
        "/edit/test_PageName",
        follow_redirects=True,
        data=dict(
            full_name="Test, Parameter",
            email="test@test.test,",
            changes="I'm a test edit!",
            contents="Here, these are the test changes!",
        ),
    )

    # Ensures page loads and changes are implemented
    assert good_resp.status_code == 200
    assert b"Here, these are the test changes!" in good_resp.data

    # Ensures changes are written into proper log file
    with open(LOG_DIR / "test_PageName.csv", "r") as f:
        expected_history = f.read()

    assert "Test, Parameter" in expected_history
    assert "test@test.test" in expected_history
    assert "I'm a test edit!" in expected_history

    # Change page back to original state
    with open(PAGE_DIR / "test_PageName.txt", "w") as f:
        f.write(revert)


def test_make_edits_invalid(client):
    # Tests a response that doesn't have all fields filled in
    bad_resp = client.post(
        "/edit/test_PageName",
        follow_redirects=True,
        data=dict(
            full_name="John Cena", email="", changes="", contents="# You cant see me!"
        ),
    )

    # Ensures form page is loaded, and changes made are still present in
    # text box
    assert bad_resp.status_code == 200
    assert b"Enter your changes:" in bad_resp.data
    assert b"# You cant see me!" in bad_resp.data


def test_make_edits_fp_append(client):
    front_page = PAGE_DIR / "FrontPage.txt"
    # Ensures page is written to front page if one doesnt
    # exist.

    # to restore front page to original state later
    with open(front_page, "r") as f:
        revert = f.read()

    resp = client.post(
        "/edit/test_NotInFP",
        follow_redirects=True,
        data=dict(
            full_name="Test, Parameter",
            email="test@test.test",
            changes="I'm a test edit!",
            contents="# Test for test_make_edits_fp_append",
        ),
    )

    # Ensure the link is present in the front page
    with open(front_page, "r") as f:
        expected = f.read()
    resp = client.get("/view/FrontPage")
    assert resp.status_code == 200
    assert "[test_NotInFP](/view/test_NotInFP)" in expected

    # Restore original front page
    with open(front_page, "w") as f:
        f.write(revert)


def test_view_history(client):
    # Tests to see if parameters from test_make_edits are present
    resp = client.get(
        "/history/test_PageName",
        follow_redirects=True,
    )
    # resp.data escapes some special characters,
    # so we test to see if their escaped versions appear
    # in resp.data
    assert resp.status_code == 200
    assert b"Test, Parameter" in resp.data
    assert b"test@test.test" in resp.data
    assert b"I&#39;m a test edit!" in resp.data


def test_view_history_no_log(client):
    resp = client.get("/history/batman", follow_redirects=True)
    assert b"No History has been found!" in resp.data
