# Marvel Cinematic Universe Wiki
*Brought to you by Team Debian!*

The Marvel cinematic universe is composed of comic book heroes that have taken a series of movie adaptations
that include Iron Man, Captain America, Thor and many others. Learn all about the Marvel Universe right here.

## Add A Page:
[Add Page](/edit/)

## View Pages:

[The_Avengers](/view/The_Avengers)[Captain_America](/view/Captain_America)
[Captain_America:_Civil_War](/view/Captain_America:_Civil_War)
