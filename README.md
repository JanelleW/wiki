# Debian Wiki - Built with Python and Flask

A wiki server that uses text files on the local filesystem to store page data. For avid and new Marvel movie watchers to access information related to cast, character, plot and theories of the movies.

## Screenshots

![A screenshot of the wiki home page. Screenshot includes a page title (Marvel Cinematic Universe Wiki). Text includes a description of the Marvel Cinematic Universe. There is a button titled "add page" and buttons that links to the pages in the wiki.](screenshots/Homepage_screenshot.png)

![A screenshot of the wiki page viewer. Screenshot includes a page title (Captain America). Text includes a biography and related movies. Includes links to Captain America: Civil War. Also has buttons for home, edit page, and view history.](screenshots/View_page_screenshot.png)

![A screenshot of the wiki edit page. Screenshot includes a page title (Currently Editing: Captain America). Edit form includes fields for editing changes, writing a change description, editor's name and email. Includes submit button.](screenshots/Edit_page_screenshot.png)

![A screenshot of the wiki add page. Screenshot includes a field for user to enter a new page name. Includes submit button.](screenshots/Add_page_screenshot.png)

![A screenshot of the wiki page history. Screenshot includes a title (Captain America Edit History). Includes a "back" button that links to the page. Text includes edit history for the page.](screenshots/Page_history_screenshot.png)

## Installation

This project has been tested with Python 3.7.3. To install the necessary dependencies, first create and activate a virtual environment.

```
# Create a directory to store virtual environments.
mkdir "$HOME/venvs"

# Create the virtual environment.
python3 -m venv "$HOME/venvs/dev"

# Activate the virtual environment.
# This must be done every time you open a terminal.
# You may want to add this to your .bashrc file.
source "$HOME/venvs/dev/bin/activate"
```

Install the necessary dependencies with pip.

```
pip install -r requirements.txt
```

## Usage

Run the web server.

```
./run-flask.sh
```

Access the wiki by opening a web preview browser tab on port 8080.

## License

MIT, see [LICENSE.txt](LICENSE.txt).
