from flask import Flask, request, render_template, redirect, Markup, url_for
import os
import markdown
import datetime
import csv

app = Flask(__name__)


@app.route("/")
def main():
    return redirect(url_for("handle_request"))


def load_page_text(filepath):
    with open(filepath) as f:
        content = f.read()
    return content


def parse_page_text(filepath):
    page_text = load_page_text(filepath)
    HTML_text = markdown.markdown(page_text)
    return HTML_text


def security_check(content: str) -> str:
    while "<script>" in content or "<post>" in content:
        if "<script>" in content:
            start_index = content.find("<script>")
            end_index = content.find("</script>") + len("</script>")
            content = content.replace(content[start_index:end_index], "")

        if "<post>" in content:
            start_index = content.find("<post>")
            end_index = content.find("</post>") + len("</post>")
            content = content.replace(content[start_index:end_index], "")
    return content


@app.route("/view/")
@app.route("/view/<string:page_name>")
def handle_request(page_name="FrontPage"):
    filepath = os.path.join("pages", f"{page_name}.txt")
    if page_name == "FrontPage":
        filtered = security_check(parse_page_text(filepath))
        return render_template(
            "main.html", page_name=page_name, page_content=Markup(filtered)
        )

    if os.path.isfile(filepath):
        filtered = security_check(parse_page_text(filepath))
        # Markup converts a string into valid HTML

        return render_template(
            "page.html", page_name=page_name, page_content=Markup(filtered)
        )

    else:
        return render_template("page_not_found.html", page_name=page_name), 404


@app.route("/api/v1/page/<page_name>/get")
def page_api_get(page_name):
    page_format = request.args.get("format", "all")
    json_response = {}
    if not os.path.exists(os.path.join("pages", f"{page_name}.txt")):
        json_response["reason"] = "Page does not exists."
        json_response["success"] = "false"
        status_code = 404
        return json_response, status_code
    if page_format == "raw":
        filepath = os.path.join("pages", f"{page_name}.txt")
        json_response["success"] = "true"
        json_response["raw"] = load_page_text(filepath)
    elif page_format == "html":
        json_response["success"] = "true"
        filepath = os.path.join("pages", f"{page_name}.txt")
        json_response["html"] = parse_page_text(filepath)
    elif page_format == "all":
        json_response["success"] = "true"
        filepath = os.path.join("pages", f"{page_name}.txt")
        json_response["raw"] = load_page_text(filepath)
        json_response["html"] = parse_page_text(filepath)

    else:
        json_response["success"] = "false"
        json_response["reason"] = "Unsupported format."
        status_code = 400
        return json_response, status_code

    status_code = 200
    return json_response, status_code


@app.route("/edit/", methods=["GET"])
def add_page_form():
    return render_template(
        "add_pagename.html",
    )


@app.route("/add/", methods=["POST"])
def add_page():
    page_name = request.form["new_page"]
    page_name = page_name.replace(" ", "_")
    return redirect(url_for("get_edit_form", page_name=page_name))


@app.route("/edit/<page_name>", methods=["GET"])
def get_edit_form(page_name):
    filepath = os.path.join("pages", f"{page_name}.txt")

    # In the event a new page is passed through this function,
    # open the file in append mode to create it then close it
    # afterwards
    f = open(filepath, "a")
    f.close()

    con = load_page_text(filepath)
    return render_template("form.html", current_contents=Markup(con), page=page_name)


@app.route("/edit/<page_name>", methods=["POST"])
def make_edits(page_name):
    # Obtain form data
    full_name = request.form["full_name"]
    email = request.form["email"]
    changes = request.form["changes"]
    con = request.form["contents"]

    if full_name and email and changes:
        # Filter the contents, then update file
        con = security_check(con)
        fullpath = os.path.join("pages", f"{page_name}.txt")

        with open(fullpath, "w") as f:
            f.write(con)

        # Get the current log path, current time, and combine
        # time, email, full name, and changes into a string
        logpath = os.path.join("logs", f"{page_name}.csv")
        time = str(datetime.datetime.now().replace(microsecond=0))

        # Place user info into an organzied string
        edit_info = [time, email, full_name, changes]

        # Write it to file
        with open(logpath, "a", newline="") as f:
            writer = csv.writer(f)
            writer.writerow(edit_info)

        # Places link to new page at the bottom of
        # FrontPage.txt if one is not already present
        page = "/view/" + page_name
        with open("pages/FrontPage.txt", "r") as f:
            con = f.read()
        if page not in con:
            markdown_link = f"[{page_name}]({page})"
            with open("pages/FrontPage.txt", "a") as f:
                f.write(markdown_link + "\n")
        return redirect(page)

    else:
        return render_template(
            "form.html",
            current_contents=Markup(con),
            page=page_name,
            error=Markup(
                "<script>window.alert('Remember to fill in all fields!')</script>"
            ),
        )


@app.route("/history/<page_name>")
def view_history(page_name):
    logpath = os.path.join("logs", f"{page_name}.csv")
    if not os.path.isfile(logpath):
        return render_template("no_history.html", page_name=page_name)

    with open(logpath, "r") as f:
        con = f.read()

    # Returns a list of the file contents to history template,
    # for loop within template displays all the information.
    return render_template("history.html", page_name=page_name, content=con)
